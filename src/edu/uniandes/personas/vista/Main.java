package edu.uniandes.personas.vista;

import edu.uniandes.personas.controlador.Controlador;
import edu.uniandes.personas.modelo.GrupoPersonas;
import edu.uniandes.personas.modelo.Modelo;


public class Main {
	
	public static void main(String[] args) {
		
		Vista v= new VistaImpl();
		GrupoPersonas mod= new GrupoPersonas();
		
		//El controlador maneja la interacción entre vista y modelo
		Controlador con= new Controlador(v, (Modelo) mod);
		
		Interfaz i=new Interfaz(con.darLista());
		//La vista observa los cambios del modelo
		mod.addObserver(v);
		
		con.correrPrograma(i);
		
	}
	
	
	

}
