package edu.uniandes.personas.vista;

import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

import edu.uniandes.personas.modelo.Persona;

public class VistaImpl implements Vista {
	
	
	private Scanner sc;
	
	public VistaImpl() {
		sc= new Scanner(System.in);
	}


	@Override
	public void update(Observable o, Object arg) {
		
		System.out.println("Modelo cambi�");
		Persona[] personasAMostrar=(Persona[]) arg;
		dibujarArreglo(personasAMostrar);
		
	}
	
	public void dibujarArreglo(Persona[] personasAMostrar) {
		for (int i = 0; i < personasAMostrar.length; i++) {
			Persona act=personasAMostrar[i];
			System.out.println("Posici�n "+i+":  "+act.darNombre()+" "+act.darEdad());
		}
	}

	@Override
	public void mostrarMenu() {
		System.out.println("--------Ejemplo MVC--------");
		System.out.println("-----Men� de aplicaci�n----");
		System.out.println("1. Cambiar datos persona");
		System.out.println("2. Dar estado");
		System.out.println("3. Salir");
		
	}

	@Override
	public int pedirOpcion() {

		int entrada=sc.nextInt();
		return entrada;
	
	}

	@Override
	public String pedirString(String mensaje) {
		System.out.println(mensaje);
		return sc.next();
		
	}

	@Override
	public int pedirEntero(String mensaje) {
		System.out.println(mensaje);
		int entrada=sc.nextInt();
		return entrada;
	}
	

}
