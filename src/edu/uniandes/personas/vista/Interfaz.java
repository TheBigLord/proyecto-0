package edu.uniandes.personas.vista;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;

import edu.uniandes.personas.modelo.Persona;

public class Interfaz extends JFrame{
	JLabel lbl1;
	JLabel lbl2;
	JLabel lbl3;
	
	public Interfaz(Persona[] x){
		
		lbl1=new JLabel("persona 1: " +x[0].darNombre()+" "+x[0].darEdad());
		lbl2=new JLabel("persona 2: " +x[1].darNombre()+" "+x[1].darEdad());
		lbl3=new JLabel("persona 3: " +x[2].darNombre()+" "+x[2].darEdad());
		
		this.setLayout(new BorderLayout());
		this.add(lbl1, BorderLayout.NORTH);
		this.add(lbl2, BorderLayout.CENTER);
		this.add(lbl3, BorderLayout.SOUTH);
		this.setSize(new Dimension(150,80));
		this.setVisible(true);
		
	}
	
	public void update(Persona[] x){
		lbl1.setText("persona 1" +x[0].darNombre()+" "+x[0].darEdad());
		lbl2.setText("persona 2" +x[1].darNombre()+" "+x[1].darEdad());
		lbl3.setText("persona 3" +x[2].darNombre()+" "+x[2].darEdad());
	}
}
