package edu.uniandes.personas.vista;

import java.util.Observer;

import edu.uniandes.personas.modelo.Persona;

public interface Vista extends Observer{

	public void mostrarMenu();

	public int pedirOpcion();

	public String pedirString(String mensaje);

	public int pedirEntero(String mensaje);
	
	public void dibujarArreglo(Persona[] personasAMostrar);
	
	
}
