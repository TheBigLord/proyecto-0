package edu.uniandes.personas.controlador;

import edu.uniandes.personas.modelo.Modelo;
import edu.uniandes.personas.modelo.Persona;
import edu.uniandes.personas.vista.Interfaz;
import edu.uniandes.personas.vista.Vista;

public class Controlador {

	private Vista miVista;
	private Modelo miModelo;
	
	public Controlador(Vista vis,Modelo mod){
		
		miVista=vis;
		miModelo=mod;
		
	}
	
	public void correrPrograma(Interfaz i){
		boolean salir=false;
		
		while(!salir){
			
			miVista.mostrarMenu();
			int entrada=miVista.pedirOpcion();
			
			
			switch (entrada) {
			case 1:
				int pos=miVista.pedirEntero("Ingrese posici�n a cambiar");
				String nombre=miVista.pedirString("Ingrese el nuevo nombre");
				int edad=miVista.pedirEntero("Ingrese la edad de la persona");
				miModelo.cambiarPersona(pos, nombre, edad);
				i.update(darLista());
				break;

			case 2:
				Persona[] arreglo=miModelo.darPersonas();
				miVista.dibujarArreglo(arreglo);
				break;
			case 3:
				
				salir=true;
				miVista.pedirString("Enter para salir ");
				
				
				break;
			default:
				break;
			}
		
		}
	}
	
	public Persona[] darLista(){
		return miModelo.darPersonas();
	}
}
