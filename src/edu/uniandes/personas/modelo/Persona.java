package edu.uniandes.personas.modelo;

public class Persona {

	private String nombre;
	
	private int edad;
	
	public Persona(String nombre, int edad) {
		super();
		this.nombre = nombre;
		this.edad = edad;
	}
	
	public String darNombre() {
		return nombre;
	}

	public int darEdad() {
		return edad;
	}

	
	
	
}
