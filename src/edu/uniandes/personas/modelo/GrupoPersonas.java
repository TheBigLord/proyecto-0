package edu.uniandes.personas.modelo;

import java.util.Observable;

public class GrupoPersonas extends Observable implements Modelo{

	private Persona[] personas;
	
	
	public GrupoPersonas(){
		personas= new Persona[3];
		personas[0]= new Persona("Pedro",10);
		personas[1]= new Persona("Miguel",12);
		personas[2]= new Persona("Pablo",15);
	}
	
	
	public Persona[] darPersonas(){
		return personas;
	}
	
	public void cambiarPersona(int pos, String nombre, int edad){
		personas[pos]= new Persona(nombre,edad);
		
		setChanged();
		notifyObservers(darPersonas());
		
	}
}
